### 1.0.0 (2022-12-08)

* Initial release

### 1.0.1 (2022-12-09)

* Added displaying an en error message in index.php so that exceptions can be diagnosed