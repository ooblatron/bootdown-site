<?php

namespace Site;

/**
 * Map Example
 * Add items to the MAP constant array, below. The URL path comes first, followed by the document path.
 * For example, to map the URL /about to the document ABOUTUS.md, add
 *      'about' => 'ABOUTUS.md'
 */
class MapExample
{
    const MAP = [
        '/'             => 'home.example.md',
    ];
}