## Bootdown ##

Congratulations, your [Bootdown](https://ooblatron.org/bootdown/) website is working!

To change this page edit `docs/home.example.md`.

To get started with your own pages
1. Make a copy of `docs/home.example.md` and call it `docs/home.md`.
2. Make a copy of `config/MapExample.php` and call it `config/Map.php`.
3. Edit `config/Map.php` and change the class name from `MapExample` to `Map`.
4. Make a copy of `config/.env.example` and call it `config/.env`.
5. In your shell execute `composer dump-autoload -o` 
6. Edit your homepage `docs/home.md`.

Add new pages by uploading Markdown documents to the docs directory and then mapping them to a URL by changing the `config/Map.php` file.

For example

1. Upload a Markdown document called `world.md` to the docs directory
2. Add the following line to `config/Maps.php`

```php
    'hello'  => 'world.md'
```

Going to `https://yourdomain.com/hello` will display the contents of `world.md` using [Bootstrap](https://getbootstrap.com/) styling.

You will probably prefer to have the name of the Markdown file match the URI path but, as the example shows, you do not have to.

You can also map to Markdown files in subdirectories, for example

```php
    'hello'  => 'greetings/world.md'
```

This displays the Markdown file `docs/greetings/world.md` when visiting `https://yourdomain.com/hello`.

### How to use Markdown ###

Please refer to the [Markdown Guide](https://www.markdownguide.org/basic-syntax/) if you need help with formatting documents.

### More information ###

- [Documentation](https://ooblatron.org/bootdown/)
- [Packagist page](https://packagist.org/packages/ooblatron/bootdown)
- [Source code](https://bitbucket.org/ooblatron/bootdown)
