<?php

use Slipstream\SlipstreamFactory;

try {
    $vendorPath = '../vendor/'; // Relative to calling script
    $classLoader = require $vendorPath . 'autoload.php';
    $factory = new SlipstreamFactory($classLoader);
    $factory->get('Bootdown')->go();
} catch (Throwable $e) {
    http_response_code(500);
    echo '<h1>500 Server Error</h1><hr>'.PHP_EOL;
    echo $e;
}