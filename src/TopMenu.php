<?php

namespace Site;

use Bootdown\Viewport;
use Exception;

class TopMenu extends Viewport
{
    /**
     * @throws Exception
     */
    public function build(): TopMenu {
        $this->startNavBar();
        $this->startNavBarRow('bg-dark');
        $this->addNavBarBrand('Bootdown');
        $this->endNavBarRow();
        $this->endNavBar();
        return $this;
    }
}