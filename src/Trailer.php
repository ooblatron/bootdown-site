<?php

namespace Site;

use Bootdown\Viewport;
use Exception;

class Trailer extends Viewport
{
    /**
     * @throws Exception
     */
    public function build(): Trailer {
        $this->addH1('Top menu');
        return $this;
    }
}